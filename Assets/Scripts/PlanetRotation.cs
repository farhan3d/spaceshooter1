﻿using UnityEngine;
using System.Collections;

public class PlanetRotation : MonoBehaviour {

	public static PlanetRotation ptr;
    public float rotationSpeed;
    public bool rotatePlanet;
    Transform childPlanet;
    
    void Awake( )
    {
        ptr = this;
        childPlanet = this.transform.GetChild( 0 ).transform;
    }
    
    IEnumerator InitiateRotation( )
    {
        rotatePlanet = true;
        while( rotatePlanet )
        {
            childPlanet.Rotate( Vector3.left * Time.deltaTime * rotationSpeed, Space.World ); 
            yield return new WaitForSeconds( 0.01f );
        }
    }
    
    public void InitiateRotationMain( )
    {
        StartCoroutine( "InitiateRotation" );
    }
}
