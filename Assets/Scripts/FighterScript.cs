﻿using UnityEngine;
using System.Collections;

public class FighterScript : MonoBehaviour {

	void Update( )
    {
        if ( Input.GetKey( "left" ) )
        {
            print( "turning left" );
        }
        
        if ( Input.GetKey( "right" ) )
        {
            print( "turning right" );
        }
        
        if ( Input.GetKeyUp( "left" ) )
        {
            print( "stopped turning left" );
        }
        
        if ( Input.GetKeyUp( "right" ) )
        {
            print( "stopped turning right" );
        }
    }
}
